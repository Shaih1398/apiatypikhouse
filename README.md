# Node.js – JWT Authentication & Authorization example with JSONWebToken & Sequelize

## Project setup
```
npm install
```

### Run
```
node server.js
```

Don't forget to change database auth on localhost

Pour les calls api, afin de pouvoir les illustrer facilement on pourra mettre ca ici.
Dans le meilleur des cas, on fera un swagger


Les commandes a faire en local pour deployer (Il faut configurer aws cli avant)
- rm testeb.zip
- zip -r "testeb.zip" * .*
- aws s3 rm --recursive s3://atypicapi
- aws s3 cp testeb.zip s3://atypicapi
