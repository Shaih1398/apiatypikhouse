const db = require("../models");
const User = db.user;
const Post = db.post;
const Comment = db.comment;
const Booking = db.booking;
const House = db.house;
const Equipment = db.equipment;
const ImgHouse = db.houseImg;
const { sendEmailContact } = require('../mailer/mailer')


exports.allAccess = (req, res) => {
	res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
	res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
	res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
	res.status(200).send("Moderator Content.");
};


exports.getAllUser = async (req, res) => {
	const allUsers = await User.findAll();
	res.status(200).send(allUsers);
};

exports.findUserById = async (req, res) => {
	const userId = req.userId;
	const user = await User.findOne({where: {id: userId}});
	res.status(200).send(user);
};
exports.findUserByHash = async (req,res) =>{
	const {hash} = req.params;
	try {
		const user = await User.findOne({where: {confirmationCode : hash}});

		user.isValid = true;
		await user.save();
		res.json({message: `User ${hash} has been activated`})
	} catch {
		res.status(422).send('User cannot be activated!');
	}

}
exports.findPostById = async (req, res) => {
	const post = await Post.findOne({where: {id: req.params.id}});
	res.status(200).send(post);
};

exports.findPostByUserId = async (req, res) => {
	const userId = req.userId;
	const post = await Post.findAll({where: {userId: userId}});
	res.status(200).send(post);
};

exports.findCommentByIdUser = async (req, res) => {
	const userId = req.userId;
	const comment = await Comment.findAll({where: {userId: userId}});
	res.status(200).send(comment);
};

exports.findAllBookingByIdUser = async (req, res) => {
	const userId = req.userId;
	const allBooking = await Booking.findAll({where: {userId: userId}, include: [House, Comment]});
	res.status(200).send(allBooking);
};

exports.findAllBookingByIdUserAndIdBooking = async (req, res) => {
	const userId = req.userId;
	const idBooking = req.params.id;
	const booking = await Booking.findAll({where: {userId: userId, bookingId: idBooking}});
	res.status(200).send(booking);
};

exports.findMyHouse = async (req, res) => {
	const userId = req.userId;
	const myHouse = await House.findAll({where: {userId: userId},  include: ImgHouse});
	res.status(200).send(myHouse);
};

exports.addRoles = async (req, res) => {
	try {
		const myHouse = await House.findAll({where: {userId: 1}, include: Equipment});
		res.status(200).send({status: "OK", myHouse});
	} catch (e) {
		res.status(400).send({status: "KO"});
	}
};
exports.sendMailContact = async (req, res) => {
	if (!req.body.email || !req.body.content) {
		return res.status(401).send({
			message: "Complétez toutes les informations demandées svp!"
		});
	}
	try {
		 await sendEmailContact(req.body.email, req.body.content)  ;
		res.status(200).send({message: "Message bien envoyé"});
	} catch (e) {
		res.status(400).send({status: "KO"});
	}
};
/*
*
* Toutes les fonctions users
*
*
* Récupérer les informations du user
* user by id // ok avec token pour user lambda
* Ses posts // OK avec token
* posts by id // OK
* commentaires by id
* Booking by idUser
* Booking by idBooking
*
* */
