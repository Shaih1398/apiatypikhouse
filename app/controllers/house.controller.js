const db = require("../models");
const Booking = db.booking;
const House = db.house;
const HouseImg = db.houseImg;
const UnavailabilityHouse = db.unavailabilityHouse;
const DynamicPropertyHouse = db.dynamicPropertiesHouse;
const { Op } = require('sequelize');
const AWS = require('aws-sdk');
const fs = require('fs');
const FileType = require('file-type');
const multiparty = require('multiparty');
const { sendConfirmationEmail, sendBannedMail, sendBookingConfirmation } = require('../mailer/mailer')

exports.cancelHouse = async (req, res) => {
    const houseId = req.params.id;
    const updateHouse = await House.update({ isBlock: true }, { where: { id: houseId } });
    res.status(200).send(updateHouse);
};

//Get Booking For one house between two date by House
//Not working to test
exports.bookingAvailableHouse = async (req, res) => {
    const houseId = req.params.id;
    const beginDate = req.params.beginDate;
    const endDate = req.params.endDate;

    const startedDate = new Date(beginDate);
    const endedDate = new Date(endDate);

    const getBookingBetweenDate = await Booking.findAll({
        where: {
            houseId: houseId,
            confirm: true,
            [Op.or]: [
                { startDate: { [Op.between]: [startedDate, endedDate] } },
                { endDate: { [Op.between]: [startedDate, endedDate] } }
            ]
        }
    });

    const getInvalidityDate = await UnavailabilityHouse.findAll({
        where: {
            houseId: houseId,
            date: { [Op.between]: [startedDate, endedDate] },
        }
    });

    if (getBookingBetweenDate.length || getInvalidityDate.length) {
        console.log("Deja Booké. Impossible de reserver !");
        res.status(200).send({ canBook: false });
    } else {
        res.status(200).send({ canBook: true });
    }
};

exports.createUnaivalabilityHouse = async (req, res) => {
    try {
        if (!req.body || !req.body.houseId || !req.body.date) {
            res.status(401).send({ message: "Impossible d'ajouter une date verfiez les champs" });
        }
        for (let date of req.body.date) {
            await UnavailabilityHouse.create({
                houseId: req.body.houseId,
                date: date
            });
        }
        res.status(200).send(response);
    } catch (e) {
        res.status(200).send({ message: "Error!" });

    }
}

exports.getAllUnaivalabilityHouse = async (req, res) => {
    try {
        if (!req.params.id) {
            res.status(401).send({ message: "Error" });
        }
        const response = await UnavailabilityHouse.findAll({ where: { houseId: req.params.id } });
        res.status(200).send(response);
    } catch (e) {
        res.status(200).send({ message: "Error!" });
    }
}

exports.deleteUnaivalabilityHouse = async (req, res) => {
    try {
        if (!req.params.id) {
            res.status(401).send({ message: "Error" });
        }
        const response = await UnavailabilityHouse.destroy({ where: { id: req.params.id } });
        res.status(200).send(response);
    } catch (e) {
        res.status(200).send({ message: "Error!" });

    }
}

// configure the keys for accessing AWS
AWS.config.update({
    accessKeyId: "AKIA2LZG6227PNVIR4CB",
    secretAccessKey: "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",
});

// create S3 instance
const s3 = new AWS.S3();

// abstracts function to upload a file returning a promise
// NOTE: if you are using TypeScript the typed function signature will be
// const uploadFile = (buffer: S3.Body, name: string, type: { ext: string; mime: string })
const uploadFile = (buffer, name, type) => {
    const params = {
        ACL: 'public-read',
        Body: buffer,
        Bucket: "imageatypic",
        ContentType: type.mime,
        Key: `${name}.${type.ext}`,
    };
    return s3.upload(params).promise();
};

exports.uploadFiles = async (request, response) => {
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, files) => {
        const house = JSON.parse(fields.house[0]);
        if (!house.name || !house.receptionCapacity || !house.price || !house.nbBed || !house.street || !house.postalCode || !house.shortDesc || !house.longDesc || !request.userId) {
            return response.status(401).send({ message: "Il manque des informations importantes !" });
        }
        const newHouse = await House.create(
            {
                name: house.name,
                receptionCapacity: house.receptionCapacity,
                price: house.price,
                nbBed: house.nbBed,
                status: house.status,
                street: house.street,
                postalCode: house.postalCode,
                city: house.city,
                country: house.country,
                userId: request.userId,
                categoryId: house.categoryId,
                shortDesc: house.shortDesc,
                longDesc: house.longDesc,
                isBlock: 1
            });

        for (let equipment of house.equipments) {
            newHouse.setEquipment(equipment.id)
        }

        if (house.dynamicProperties) {
            for (key in house.dynamicProperties) {
                if (house.dynamicProperties.hasOwnProperty(key)) {
                    let value = house.dynamicProperties[key];
                    await DynamicPropertyHouse.create({
                        value: Object.values(value)[0],
                        houseId: newHouse.id,
                        dynamicPropertyId: Object.keys(value)[0]
                    });
                }
            }
        }

        const houseId = newHouse.id;
        if (error) {
            console.log("Error ", error);
            return response.status(500).send(error);
        }

        try {
            for (let file of files.file) {
                const path = file.path;
                const buffer = fs.readFileSync(path);
                const type = await FileType.fromBuffer(buffer);
                const fileName = `images/${Date.now().toString()}`;
                const upload = await uploadFile(buffer, fileName, type);
                if (upload) {
                    HouseImg.create({ img: `${fileName}.${type.ext}`, houseId: houseId })
                }
            }
            return response.status(200).send({ upload: true });
        } catch (err) {
            return response.status(500).send(err);
        }
    });
};

exports.uploadImages = async (request, response) => {
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, files) => {
        const house = JSON.parse(fields.house[0]);
        if (!house.id) {
            return response.status(401).send({ message: "Il manque des informations importantes !" });
        }
        try {
            if (files && files.file) {
                for (let file of files.file) {
                    const path = file.path;
                    const buffer = fs.readFileSync(path);
                    const type = await FileType.fromBuffer(buffer);
                    const fileName = `images/${Date.now().toString()}`;
                    const upload = await uploadFile(buffer, fileName, type);
                    if (upload) {
                    }
                }
            }
            return response.status(200).send({ upload: true });
        } catch (err) {
            return response.status(500).send(err);
        }
    });
};

exports.addEquipmentOnHouse = async (req, res) => {
    try {
        const idHouse = req.params.id;
        const equipments = req.body.equipments;
        if (!idHouse || !equipments) {
            return res.status(401).send({ message: "Manque d'informations" });
        }
        const house = await House.findAll({ where: { id: idHouse } });
        console.log(house[0]);
        for (let equipment of equipments) {
            console.log("equipment");
            house[0].addEquipment(equipment.id)
        }

        return res.status(200).send({ addEquipment: true });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}
