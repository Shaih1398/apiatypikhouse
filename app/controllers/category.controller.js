const db = require("../models");
const User = db.user;
const Post = db.post;
const Comment = db.comment;
const Booking = db.booking;
const House = db.house;
const Category = db.category;
const Equipment = db.equipment;
const ImgHouse = db.houseImg;
const dynamicPropertiesHouses = db.dynamicPropertiesHouse;
const { sendCreateCategoryMail, sendCreateEquipementMail } = require('../mailer/mailer')
const CategoryImg = db.categoryImg;

//Recupérer toutes les categories
exports.findAllCategory = async (req, res) => {
	const categories = await Category.findAll({ include: [CategoryImg] });
	res.status(200).send(categories);
};

//Recupérer toutes les categories
exports.findAllEquipments = async (req, res) => {
	const equipments = await Equipment.findAll({});
	res.status(200).send(equipments);
};

//Recupérer toutes les categories par ID
exports.findAllCategoryById = async (req, res) => {
	const categories = await Category.findAll({ where: { id: req.params.id } });
	res.status(200).send(categories);
};

//All house by categ ID
exports.findHouseByCategory = async (req, res) => {
	const categoryId = req.params.id;
	if (!categoryId) {
		res.status(401).send({ message: "Impossible de trouver les categories" });
	}
	const houses = await House.findAll({ where: { categoryId: categoryId }, include: [Equipment, ImgHouse, Comment, User, Category] });
	res.status(200).send(houses);
};

//House by categ ID
exports.findHouseByID = async (req, res) => {
	const houseId = req.params.id;
	if (!houseId) {
		res.status(401).send({ message: "Impossible de trouver cette house" });
	}
	const house = await House.findOne({ where: { id: houseId }, include: ImgHouse });
	res.status(200).send(house);
};

//Comment By House
exports.findCommentByHouse = async (req, res) => {
	const houseId = req.params.id;
	if (!houseId) {
		res.status(401).send({ message: "Impossible de trouver les informations" });
	}
	const commentByHouse = await Comment.findAll({ where: { houseId: houseId } });
	res.status(200).send(commentByHouse);
};

//Return House with equipment
exports.findEquipmentByHouse = async (req, res) => {
	const houseId = req.params.id;
	if (!houseId) {
		res.status(401).send({ message: "Impossible de trouver des equipements" });
	}
	const myHouse = await House.findAll({ where: { id: houseId }, include: [Equipment, dynamicPropertiesHouses, ImgHouse, Comment, User] });
	res.status(200).send(myHouse);
};


//Add equipment
exports.addEquipment = async (req, res) => {
	const equipment = req.body.equipment;
	if (!equipment) {
		res.status(401).send({ message: "Impossible d'ajouter des equipements" });
	}
	let users = await User.findAll({where: {}});

	for (let user of users) {
		await sendCreateEquipementMail(user.lastName, user.firstName, user.email, equipment)
	}
	const newEquipment = await Equipment.create({ name: equipment });
	res.status(200).send(newEquipment);
};



//Add Category
exports.deleteCategory = async (req, res) => {
	const category = req.params.id;
	if (!category) {
		res.status(401).send({ message: "Suppression impossible" });
	}
	const deleteCategory = await Category.destroy({ where: { id: category } });

	if (deleteCategory) {
		res.status(200).send({ status: "OK" });
	} else {
		res.status(200).send({ status: "OK" });
	}
};


exports.deleteImgHouse = async (req, res) => {
	const idHouse = req.params.id;
	if (!idHouse) {
		res.status(401).send({ message: "Suppression impossible" });
	}
	const deleteCategory = await ImgHouse.destroy({ where: { id: idHouse } });

	if (deleteCategory) {
		res.status(200).send({ status: "OK" });
	} else {
		res.status(401).send({ status: "KO" });
	}
};

exports.deleteEquipment = async (req, res) => {
	const idEquipment = req.params.id;
	if (!idEquipment) {
		res.status(401).send({ message: "Suppression impossible" });
	}
	const deleteEquipment = await Equipment.destroy({ where: { id: idEquipment } });

	if (deleteEquipment) {
		res.status(200).send({ status: "OK" });
	} else {
		res.status(401).send({ status: "KO" });
	}
};


//Add Category
exports.updateCategory = async (req, res) => {
	const category = req.params.id;
	const name = req.body.name;
	const updateCategory = await Category.update({ name: name }, { where: { id: category } });


	res.status(200).send(updateCategory);
};

//delete house
exports.deleteHouse = async (req, res) => {
	const house = req.params.id;
	const deleteHouse = await House.destroy({ where: { id: house } });

	if (deleteHouse) {
		res.status(200).send({ status: "OK" });
	} else {
		res.status(401).send({ status: "KO" });
	}
};

// Add house
exports.addHouse = async (req, res) => {
	const house = req.body;
	if (!house) {
		return res.status(401).send({ message: "Impossible d'ajouter une house" });
	}
	const newHouse = await House.create(
		{
			name: house.name,
			receptionCapacity: house.receptionCapacity,
			price: house.price,
			nbBed: house.nbBed,
			status: 0,
			street: house.street,
			postalCode: house.postalCode,
			city: house.city,
			country: house.country,
			userId: house.userId,
			categoryId: house.categoryId
		}
	);
	//await uploadFiles(req, res, house.file);
	res.status(200).send(newHouse);
};

//update house
exports.updateHouse = async (req, res) => {
	const houseId = req.params.id;
	const house = req.body.house;
	const updateHouse = await House.update({
		name: house.name,
		receptionCapacity: house.receptionCapacity,
		price: house.price,
		nbBed: house.nbBed,
		longDesc: house.longDesc,
		shortDesc: house.shortDesc,
	}, { where: { id: houseId } });
	res.status(200).send(updateHouse);
};

//addComment
exports.addComment = async (req, res) => {
	const { rate, bookingId, houseId, comment } = req.body;
	if (!rate || !bookingId || !houseId || !comment) {
		return res.status(200).send({ message: "Impossible de laisser le commentaire. Il faut remplir tous les champs !" });
	}
	const newComment = await Comment.create(
		{
			rate: rate,
			valid: 0,
			userId: req.userId,
			bookingId: bookingId,
			houseId: houseId,
			comment: comment
		}
	);
	res.status(200).send(newComment);
};
