const db = require("../models");
const config = require("../config/auth.config");
const { sendConfirmationEmail } = require('../mailer/mailer')
const nodemailer = require('nodemailer');
const User = db.user;
const Role = db.role;


const Op = db.Sequelize.Op;

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = async (req, res) => {

	let token = Math.random().toString(36).substring(7);

	if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.password || !req.body.street || !req.body.postalCode || !req.body.city || !req.body.country) {
		return res.status(401).send({
			message: "Complétez toutes les informations demandées svp!"
		});
	}

	try {
		let user = await User.create({
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email,
			password: bcrypt.hashSync(req.body.password, 8),
			street: req.body.street,
			postalCode: req.body.postalCode,
			city: req.body.city,
			country: req.body.country,
			confirmationCode: token
		});

		let rolesForUser;
		if (req.body.roles) {
			rolesForUser = await Role.findAll({
				where: {
					name: {
						[Op.or]: req.body.roles
					}
				}
			})
		}

		if (req.body.roles && rolesForUser) {
			user.setRoles(rolesForUser).then(() => {
				res.send({ message: "User registered successfully!" });
				sendConfirmationEmail({ toUser: user })
			});
		} else {
			user.setRoles([1]).then(() => {
				res.send({ message: "User registered successfully!" });
				sendConfirmationEmail({ toUser: user })
			});
		}
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
};

exports.signin = (req, res) => {
	if (!req.body.email || !req.body.password) {
		return res.status(401).send({
			message: "Complétez toutes les informations demandées svp!"
		});
	}



	User.findOne({
		where: {
			email: req.body.email
		}
	})
		.then(user => {
			if (!user) {
				return res.status(404).send({ message: "User Not found." });
			}

			const passwordIsValid = bcrypt.compareSync(
				req.body.password,
				user.password
			);

			if (!passwordIsValid) {
				return res.status(401).send({
					accessToken: null,
					message: "Invalid Password!"
				});
			}

			const token = jwt.sign({ id: user.id }, config.secret, {
				expiresIn: 86400 // 24 hours
			});

			let authorities = [];
			user.getRoles().then(roles => {
				for (let i = 0; i < roles.length; i++) {
					authorities.push("ROLE_" + roles[i].name.toUpperCase());
				}
				res.status(200).send({
					id: user.id,
					firstName: user.firstName,
					lastName: user.lastName,
					street: user.street,
					postalCode: user.postalCode,
					city: user.city,
					country: user.country,
					email: user.email,
					roles: authorities,
					accessToken: token,
					isValid : user.isValid,
					isBlock : user.isBlock
				});
			});
		})
		.catch(err => {
			res.status(500).send({ message: err.message });
		});
};
