const db = require("../models");
const User = db.user;
const Post = db.post;
const Comment = db.comment;
const House = db.house;
const Category = db.category;
const CategoryImg = db.categoryImg;

const Equipment = db.equipment;
const DynamicProperty = db.dynamicProperties;
const { Op } = require('sequelize');
const fs = require('fs');
const FileType = require('file-type');
const multiparty = require('multiparty');
const AWS = require('aws-sdk');
const { sendEmailBlock, sendCreateCategoryMail } = require('../mailer/mailer')

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////ADMIN PART///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const uploadFile = (buffer, name, type) => {
    const params = {
        ACL: 'public-read',
        Body: buffer,
        Bucket: "imageatypic",
        ContentType: type.mime,
        Key: `${name}.${type.ext}`,
    };
    return s3.upload(params).promise();
};

// configure the keys for accessing AWS
AWS.config.update({
    accessKeyId: "AKIA2LZG6227PNVIR4CB",
    secretAccessKey: "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",
});

// create S3 instance
const s3 = new AWS.S3();

//Block User
exports.adminBlockUser = async (req, res) => {
    try {
        const idUser = req.params.id;
        const user = await User.findOne({ where: { id: idUser } });
        const statusUser = !user.isBlock;
        const userToBlock = await User.update({ isBlock: statusUser }, { where: { id: idUser } });
        await sendEmailBlock(user.email, user.firstName, user.lastName)
        res.status(200).send({ userToBlock: userToBlock });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

//List all user
exports.adminListUser = async (req, res) => {
    try {
        const users = await User.findAll({});
        res.status(200).send({ users: users });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.updateComment = async (req, res) => {
    try {
        const idComment = req.params.id;
        const commentToUpdate = await Comment.update({ valid: false, isBlock: false }, { where: { id: idComment } });
        res.status(200).send({ commentToUpdate: commentToUpdate });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.listAllComments = async (req, res) => {
    try {
        const comments = await Comment.findAll({ where: { isBlock: true } });
        res.status(200).send({ comments: comments });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.listHouse = async (req, res) => {
    try {
        const house = await House.findAll({ where: { isBlock: true } });
        res.status(200).send({ house: house });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.unblockHouse = async (req, res) => {
    try {
        const idHouse = req.params.id;
        const houseToUpdate = await House.update({ isBlock: false, status: false }, { where: { id: idHouse } });
        res.status(200).send({ houseToUpdate: houseToUpdate });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.addCategories = async (request, res) => {
    try {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            const categ = JSON.parse(fields.category[0]);
            if (!categ.name) {
                return response.status(401).send({ message: "Il manque des informations importantes !" });
            }
            const newCateg = await Category.create({ name: categ.name });
            const id = newCateg.id;

            if (files && files.file && files.file.length > 0) {
                for (let file of files.file) {
                    const path = file.path;
                    const buffer = fs.readFileSync(path);
                    const type = await FileType.fromBuffer(buffer);
                    const fileName = `images/${Date.now().toString()}`;
                    const upload = await uploadFile(buffer, fileName, type);
                    if (upload) {
                        CategoryImg.create({ img: `${fileName}.${type.ext}`, categoryId: id })
                    }
                }
            }
            let users = await User.findAll({where: {}});

            for (let user of users) {
                await sendCreateCategoryMail(user.lastName, user.firstName, user.email, category)
            }
            res.status(200).send({ created: true });
        });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}


exports.updateCategories = async (req, res) => {
    try {
        const categoryId = req.params.id;
        const nameCategory = req.body.category;
        const categToUpdate = await User.update({ name: nameCategory.name }, { where: { id: categoryId } });
        res.status(200).send({ categToUpdate: categToUpdate });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.addPost = async (request, res) => {
    let image = null;
    try {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            for (let file of files.file) {
                const path = file.path;
                const buffer = fs.readFileSync(path);
                const type = await FileType.fromBuffer(buffer);
                const fileName = `images/${Date.now().toString()}`;
                const upload = await uploadFile(buffer, fileName, type);
                if (upload) {
                    image = `${fileName}.${type.ext}`;
                }
            }
            console.log(image);
            const postOne = JSON.parse(fields.post[0]);

            const post = await Post.create(
                {
                    content: postOne.content,
                    title: postOne.title,
                    image: image
                }
            );
            res.status(200).send({ post: post });
        });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.updatePost = async (req, res) => {
    try {
        const post = req.body.post;
        const idPost = req.params.id;
        const createCateg = await Post.update(
            {
                content: post.content,
                title: post.title,
                image: post.image
            }
            , { where: { id: idPost } });
        res.status(200).send({ createCateg: createCateg });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.deletePost = async (req, res) => {
    try {
        const response = await Post.destroy({ where: { id: req.params.id } });
        res.status(200).send({ response: response });
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.listsPost = async (req, res) => {
    try {
        const posts = await Post.findAll({});
        res.status(200).send(posts);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.listPostById = async (req, res) => {
    try {
        const posts = await Post.findAll({ where: { id: req.params.id } });
        res.status(200).send(posts);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

//Propriété dynamique
exports.addPropertyDynamic = async (req, res) => {
    try {
        const propertyDynamic = await DynamicProperty.create({
            name: req.body.name,
            type: req.body.type,
            categoryId: req.body.categoryId
        });
        res.status(200).send(propertyDynamic);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

//Get all dynamic properties
exports.getAllPropertyDynamic = async (req, res) => {
    try {
        const propertyDynamics = await DynamicProperty.findAll({});
        res.status(200).send(propertyDynamics);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.getCategById = async (req, res) => {
    try {
        const category = await Category.findOne({ where: { id: req.params.id }, include: [DynamicProperty] });
        res.status(200).send(category);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.updateCategById = async (req, res) => {
    const idCateg = req.params.id;
    try {
        const category = await Category.update({ name: req.body.name }, { where: { id: idCateg } });
        res.status(200).send(category);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.addEquipment = async (req, res) => {
    try {
        const equipment = await Equipment.create({
            name: req.body.name
        });
        res.status(200).send(equipment);
    } catch (e) {
        console.log("Error : ", e)
        return res.status(401).send({ message: "Error" });
    }
}

exports.addCategory = async (req, res) => {
    const category = req.body.name;
    if (!category) {
        res.status(401).send({ message: "Impossible d'ajouter des categories" });
    }
    const newCategory = await Category.create({ name: category });


    res.status(200).send(newCategory);

};
