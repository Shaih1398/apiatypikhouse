const db = require("../models");
const User = db.user;
const Post = db.post;
const Comment = db.comment;
const Booking = db.booking;
const House = db.house;
const Category = db.category;
const Equipment = db.equipment;
const HouseImg = db.houseImg;
const UnavailabilityHouse = db.unavailabilityHouse;
const DynamicProperty = db.dynamicProperties;
const DynamicPropertyHouse = db.dynamicPropertiesHouse;
const { Op } = require('sequelize');
const AWS = require('aws-sdk');
const fs = require('fs');
const FileType = require('file-type');
const multiparty = require('multiparty');
const { sendEmailBlock , sendBookingConfirmation, sendBookingConfirmationOwner } = require('../mailer/mailer')


exports.cancelBooking = async (req, res) => {
	const bookingId = req.params.id;
	const updateBooking = await Booking.update({ confirm: false }, { where: { id: bookingId } });
	res.status(200).send(updateBooking);
};

exports.cancelHouse = async (req, res) => {
	const houseId = req.params.id;
	const updateHouse = await House.update({ isBlock: true }, { where: { id: houseId } });
	res.status(200).send(updateHouse);
};

exports.getBookingById = async (req, res) => {
	const bookingId = req.params.id;
	const booking = await Booking.findAll({ where: { id: bookingId }, include: [House, Comment] });
	res.status(200).send(booking);
};

exports.getBookingByIdHouse = async (req, res) => {
	const houseId = req.params.id;
	const bookings = await Booking.findAll({ where: { houseId: houseId } });
	res.status(200).send(bookings);
};


//Get Booking For one house between two date by House
//Not working to test
exports.bookingAvailableHouse = async (req, res) => {
	const houseId = req.params.id;
	const beginDate = req.params.beginDate;
	const endDate = req.params.endDate;

	const startedDate = new Date(beginDate);
	const endedDate = new Date(endDate);

	const getBookingBetweenDate = await Booking.findAll({
		where: {
			houseId: houseId,
			confirm: true,
			[Op.or]: [
				{ startDate: { [Op.between]: [startedDate, endedDate] } },
				{ endDate: { [Op.between]: [startedDate, endedDate] } }
			]
		}
	});

	const getInvalidityDate = await UnavailabilityHouse.findAll({
		where: {
			houseId: houseId,
			date: { [Op.between]: [startedDate, endedDate] },
		}
	});

	if (getBookingBetweenDate.length || getInvalidityDate.length) {
		console.log("Deja Booké. Impossible de reserver !");
		res.status(200).send({ canBook: false });
	} else {
		res.status(200).send({ canBook: true });
	}
};

exports.createUnaivalabilityHouse = async (req, res) => {
	try {
		if (!req.body || !req.body.houseId || !req.body.date) {
			res.status(401).send({ message: "Impossible d'ajouter une date verfiez les champs" });
		}
		for (let date of req.body.date) {
			await UnavailabilityHouse.create({
				houseId: req.body.houseId,
				date: date
			});
		}
		res.status(200).send(response);
	} catch (e) {
		res.status(200).send({ message: "Error!" });

	}
}

exports.getAllUnaivalabilityHouse = async (req, res) => {
	try {
		if (!req.params.id) {
			res.status(401).send({ message: "Error" });
		}
		const response = await UnavailabilityHouse.findAll({ where: { houseId: req.params.id } });
		res.status(200).send(response);
	} catch (e) {
		res.status(200).send({ message: "Error!" });
	}
}

exports.deleteUnaivalabilityHouse = async (req, res) => {
	try {
		if (!req.params.id) {
			res.status(401).send({ message: "Error" });
		}
		const response = await UnavailabilityHouse.destroy({ where: { id: req.params.id } });
		res.status(200).send(response);
	} catch (e) {
		res.status(200).send({ message: "Error!" });

	}
}

exports.createBookingPayment = async (req, res) => {
	const stripe = require("stripe")("sk_test_51J4CZ2Ej0EsOxVy2m9W1fZ4c4yqFKsiweh2XEWLbbs34hnuLceJcgTQg0z8MDnFmHmANrHB7FOmddgRchlfReDDD00gobNtj68");
	const { items } = req.body;
	const cancelResaId = items[0].cancelResaId;

	// Create a PaymentIntent with the order amount and currency
	const paymentIntent = await stripe.paymentIntents.create({
		amount: items[0].amount * 100,
		description: "Payment Shai de test",
		currency: "eur"
	});

	if (paymentIntent.client_secret) {
		cancelResaId && await Booking.destroy({ where: { id: cancelResaId } });

		//Add booking
		let booking = await Booking.create(
			{
				price: items[0].amount,
				startDate: items[0].begin,
				endDate: items[0].end,
				userId: req.userId,
				houseId: items[0].houseId,
				numPers: items[0].numPerson
			}
		);
		let user = await User.findOne({
			where: {
				id: booking.userId
			}
		})
		let house = await House.findOne({
			where: {
				id: booking.houseId
			}
		})
		let userOwner = await User.findOne({
			where: {
				id: house.userId
			}
		})

		sendBookingConfirmation(user.email, booking.id, house.name, booking.price, booking.startDate, booking.endDate)
		sendBookingConfirmationOwner(userOwner.email , booking.id, house.name, booking.price, booking.startDate, booking.endDate)
		res.send({
			clientSecret: paymentIntent.client_secret,
		});
	}


};











