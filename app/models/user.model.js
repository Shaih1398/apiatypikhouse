module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define("users", {
		firstName: {
			type: Sequelize.STRING
		},
		lastName: {
			type: Sequelize.STRING
		},
		email: {
			type: Sequelize.STRING
		},
		password: {
			type: Sequelize.STRING
		},
		street: {
			type: Sequelize.STRING
		},
		postalCode: {
			type: Sequelize.STRING
		},
		city: {
			type: Sequelize.STRING
		},
		country: {
			type: Sequelize.STRING
		},
		isValid: {
			type: Sequelize.BOOLEAN
		},
		confirmationCode: {
			type: Sequelize.STRING,
			defaultValue: false
		},
		isBlock : {
			type: Sequelize.BOOLEAN,
			defaultValue: false
		}
	});

	return User;
};
