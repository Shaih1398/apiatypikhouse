module.exports = (sequelize, Sequelize) => {
    const dynamicPropertiesHouse = sequelize.define("dynamicPropertiesHouse", {
        value: {
            type: Sequelize.STRING
        },
    });

    return dynamicPropertiesHouse;
};
