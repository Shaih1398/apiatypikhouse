module.exports = (sequelize, Sequelize) => {
	const equipment = sequelize.define("equipment", {
		name: {
			type: Sequelize.STRING
		},
	});

	return equipment;
};
