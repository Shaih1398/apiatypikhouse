module.exports = (sequelize, Sequelize) => {
    const CategoryImg = sequelize.define("categoryImg", {
        img: {
            type: Sequelize.STRING
        },
    });

    return CategoryImg;
};
