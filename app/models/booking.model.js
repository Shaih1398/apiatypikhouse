module.exports = (sequelize, Sequelize) => {
	const Booking = sequelize.define("booking", {
		price: {
			type: Sequelize.STRING
		},
		startDate: {
			type: Sequelize.DATE
		},
		endDate: {
			type: Sequelize.DATE
		},
		confirm: {
			type: Sequelize.BOOLEAN,
			defaultValue: true
		},
		numPers: {
			type: Sequelize.INTEGER
		}

	});

	return Booking;
};
