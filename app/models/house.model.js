module.exports = (sequelize, Sequelize) => {
	const House = sequelize.define("house", {
		name: {
			type: Sequelize.STRING
		},
		receptionCapacity: {
			type: Sequelize.INTEGER
		},
		price: {
			type: Sequelize.INTEGER
		},
		nbBed: {
			type: Sequelize.INTEGER
		},
		status: {
			type: Sequelize.STRING
		},
		street: {
			type: Sequelize.STRING
		},
		postalCode: {
			type: Sequelize.STRING
		},
		city: {
			type: Sequelize.STRING
		},
		country: {
			type: Sequelize.STRING
		},
		shortDesc : {
			type: Sequelize.STRING
		},
		longDesc : {
			type: Sequelize.STRING
		},
		isBlock: {
			type: Sequelize.BOOLEAN
		}
	});

	return House;
};
