module.exports = (sequelize, Sequelize) => {
	const unavailabilityHouse = sequelize.define("unavailabilityHouse", {
		date: {
			type: Sequelize.DATE
		},
	});

	return unavailabilityHouse;
};
