module.exports = (sequelize, Sequelize) => {
	const HouseImg = sequelize.define("houseImg", {
		img: {
			type: Sequelize.STRING
		},
	});

	return HouseImg;
};
