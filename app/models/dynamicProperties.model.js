module.exports = (sequelize, Sequelize) => {
	const DynamicProperties = sequelize.define("dynamicProperties", {
		name: {
			type: Sequelize.STRING
		},
		type: {
			type: Sequelize.STRING
		},
	});

	return DynamicProperties;
};
