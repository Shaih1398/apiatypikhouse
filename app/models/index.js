const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
	config.DB,
	config.USER,
	config.PASSWORD,
	{
		host: config.HOST,
		dialect: config.dialect,
		operatorsAliases: false,

		pool: {
			max: config.pool.max,
			min: config.pool.min,
			acquire: config.pool.acquire,
			idle: config.pool.idle
		}
	}
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.post = require("../models/post.model.js")(sequelize, Sequelize);
db.house = require("../models/house.model.js")(sequelize, Sequelize);
db.comment = require("../models/comment.model.js")(sequelize, Sequelize);
db.booking = require("../models/booking.model.js")(sequelize, Sequelize);
db.categoryImg = require("../models/categoryImg.model.js")(sequelize, Sequelize);
db.equipment = require("../models/equipment.model.js")(sequelize, Sequelize);
db.houseImg = require("../models/houseImg.model.js")(sequelize, Sequelize);
db.unavailabilityHouse = require("../models/unavailabilityHouse.model.js")(sequelize, Sequelize);
db.category = require("../models/category.model.js")(sequelize, Sequelize);
db.dynamicProperties = require("../models/dynamicProperties.model.js")(sequelize, Sequelize);
db.dynamicPropertiesHouse = require("../models/dynamicPropertiesHouse.model.js")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
	through: "user_roles",
	foreignKey: "roleId",
	otherKey: "userId"
});

db.user.belongsToMany(db.role, {
	through: "user_roles",
	foreignKey: "userId",
	otherKey: "roleId"
});

db.post.belongsTo(db.user);
db.house.belongsTo(db.user);

db.booking.belongsTo(db.user);
db.booking.belongsTo(db.house);
db.booking.hasOne(db.comment);

db.comment.belongsTo(db.user);
db.comment.belongsTo(db.booking);
db.comment.belongsTo(db.house);
db.house.hasMany(db.comment);

db.house.hasMany(db.houseImg);
db.house.hasMany(db.dynamicPropertiesHouse);

db.unavailabilityHouse.belongsTo(db.house);

db.house.belongsTo(db.category);
db.category.hasMany(db.dynamicProperties);
db.category.hasMany(db.categoryImg);
db.dynamicProperties.belongsTo(db.category);
db.dynamicPropertiesHouse.belongsTo(db.dynamicProperties);

db.equipment.belongsToMany(db.house, {
	through: "equipment_house",
	foreignKey: "equipmentId",
	otherKey: "houseId"
});

db.house.belongsToMany(db.equipment, {
	through: "equipment_house",
	foreignKey: "houseId",
	otherKey: "equipmentId"
});



/*db.house.belongsToMany(db.dynamicProperties, {
	through: "dynamic_value",
	foreignKey: "houseId",
	otherKey: "propertyId"
});

db.dynamicProperties.belongsToMany(db.house, {
	through: "dynamic_value",
	foreignKey: "propertyId",
	otherKey: "houseId"
});*/

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;
