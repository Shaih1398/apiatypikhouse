module.exports = (sequelize, Sequelize) => {
	return Comment = sequelize.define("comment", {
		rate: {
			type: Sequelize.INTEGER
		},
		comment: {
			type: Sequelize.STRING
		},
		valid: {
			type: Sequelize.BOOLEAN
		},
		isBlock: {
			type: Sequelize.BOOLEAN,
			defaultValue: false
		}
	});
};
