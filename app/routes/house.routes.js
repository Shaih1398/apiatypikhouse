const { authJwt } = require("../middleware");
const controller = require("../controllers/house.controller");

module.exports = function (app) {

    app.get("/api/booking/available/:id/:beginDate/:endDate", controller.bookingAvailableHouse);
    app.post("/api/upload/files", [authJwt.verifyToken], controller.uploadFiles);
    app.post("/api/addImage", [authJwt.verifyToken], controller.uploadImages);
    app.post("/api/addEquipment/:id", [authJwt.verifyToken], controller.addEquipmentOnHouse);
    app.put("/api/cancel/house/:id", [authJwt.verifyToken], controller.cancelHouse);
    app.post("/api/unavailability/house", [authJwt.verifyToken], controller.createUnaivalabilityHouse);
    app.get("/api/unavailability/:id", [authJwt.verifyToken], controller.getAllUnaivalabilityHouse);
    app.delete("/api/unavailability/:id", [authJwt.verifyToken], controller.deleteUnaivalabilityHouse);
};
