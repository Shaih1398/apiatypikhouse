const { authJwt } = require("../middleware");
const controller = require("../controllers/category.controller");

module.exports = function (app) {
	app.get("/api/category", controller.findAllCategory);
	app.get("/api/equipments", controller.findAllEquipments);
	app.get("/api/category/:id", controller.findAllCategoryById);
	app.get("/api/categoryHouse/:id", controller.findHouseByCategory);
	app.get("/api/houseById/:id", controller.findHouseByID);
	app.get("/api/comment/house/:id", controller.findCommentByHouse);
	app.get("/api/house/:id", controller.findEquipmentByHouse);
	app.delete("/api/category/:id", controller.deleteCategory);
	app.put("/api/category/:id", controller.updateCategory);
	app.delete("/api/deleteHouseImg/:id", [authJwt.verifyToken], controller.deleteImgHouse);
	app.delete("/api/deleteEquipment/:id", [authJwt.verifyToken], controller.deleteEquipment);
	app.post("/api/account/comment", [authJwt.verifyToken], controller.addComment);

};
