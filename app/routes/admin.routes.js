const { authJwt } = require("../middleware");
const controller = require("../controllers/admin.controller");

module.exports = function (app) {


app.put("/api/admin/blockUser/:id",  controller.adminBlockUser);
app.get("/api/admin/users",  controller.adminListUser);
app.put("/api/admin/comment/:id",  controller.updateComment);
app.get("/api/admin/comments",  controller.listAllComments);
app.get("/api/admin/houses",  controller.listHouse);
app.put("/api/admin/unblockHouse/:id", controller.unblockHouse);
app.post("/api/admin/addCategory",  controller.addCategories);
app.put("/api/admin/updateCategories/:id",  controller.updateCategories);
app.get("/api/admin/posts",  controller.listsPost, );
app.put("/api/admin/posts/:id",  controller.updatePost );
app.post("/api/admin/addPosts",  controller.addPost);
app.delete("/api/admin/deletePosts/:id",  controller.deletePost);
app.get("/api/admin/posts/:id",  controller.listPostById);
app.post("/api/admin/addPropertyDynamic", controller.addPropertyDynamic);
app.get("/api/admin/propertyDynamic",   controller.getAllPropertyDynamic);
app.get("/api/admin/category/:id",   controller.getCategById);
app.put("/api/admin/updateCategById/:id",  controller.updateCategById);
app.post("/api/admin/addEquipment",  controller.addEquipment);

};