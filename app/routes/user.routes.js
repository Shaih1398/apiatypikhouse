const {authJwt} = require("../middleware");
const controller = require("../controllers/user.controller");
const controllerCateg = require("../controllers/category.controller");

module.exports = function (app) {
	app.use(function (req, res, next) {
		res.header(
			"Access-Control-Allow-Headers",
			"x-access-token, Origin, Content-Type, Accept"
		);
		next();
	});

	app.get("/api/test/all", controller.allAccess);

	app.get(
		"/api/test/user",
		[authJwt.verifyToken],
		controller.userBoard
	);

	app.get(
		"/api/test/mod",
		[authJwt.verifyToken, authJwt.isModerator],
		controller.moderatorBoard
	);

	app.get(
		"/api/test/admin",
		[authJwt.verifyToken, authJwt.isAdmin],
		controller.adminBoard
	);

	app.get(
		"/api/users/all",
		controller.getAllUser
	);

	app.get(
		"/api/user/me",
		[authJwt.verifyToken],
		controller.findUserById
	);

	app.get(
		"/api/posts",
		[authJwt.verifyToken],
		controller.findPostByUserId,
	);

	app.get(
		"/api/posts/:id",
		controller.findPostById
	);

	app.get(
		"/api/comment",
		[authJwt.verifyToken],
		controller.findCommentByIdUser
	);

	app.get(
		"/api/house/me",
		[authJwt.verifyToken],
		controller.findMyHouse
	);

	app.post(
		"/api/sendMailContact",
		controller.sendMailContact
	);

	app.get(
		"/api/booking/me",
		[authJwt.verifyToken],
		controller.findAllBookingByIdUser
	);

	app.get(
		"/api/addRoles", controller.addRoles
	);

    app.get(
    	"/api/activate/:hash", controller.findUserByHash
	)
};
