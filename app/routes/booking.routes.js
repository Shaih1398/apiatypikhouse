const { authJwt } = require("../middleware");
const controller = require("../controllers/booking.controller");

module.exports = function (app) {
	app.get("/api/booking/available/:id/:beginDate/:endDate", controller.bookingAvailableHouse);
	app.post("/api/booking/createPayment", [authJwt.verifyToken], controller.createBookingPayment);
	app.put("/api/booking/:id", [authJwt.verifyToken], controller.cancelBooking);
	app.get("/api/booking/:id", [authJwt.verifyToken], controller.getBookingById);
	app.get("/api/bookingHouse/:id", [authJwt.verifyToken], controller.getBookingByIdHouse);

};
