const nodemailer = require('nodemailer');



function sendEmail(message) {
    return new Promise((res, rej) => {
        const transporter = nodemailer.createTransport({

            host: 'smtp.mail.us-east-1.awsapps.com',
            secure:  true,
            port: 465,
            auth: {
                user: 'shai@dsp-dev019-sh-ri-de.fr',
                pass: 'ShaiAtypic3'
            },

        })

        transporter.sendMail(message, function(err, info) {
            if (err) {
                rej(err)
            } else {
                res(info)
            }
        })
    })
}

exports.sendConfirmationEmail = function({toUser}) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${toUser.email}` ,
        subject: 'Activation du compte AtypikHouse',
        html: `
      <h3> Bonjour ${toUser.lastName} ${toUser.firstName} </h3>
      <p>Merci pour votre demande d'inscription</p>
      <p>Pour activer votre compte merci de bien vouloir cliquer  <a target="_" href="https://dsp-dev019-sh-ri-de.fr/verifyEmail/${toUser.confirmationCode}">Ici</a></p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendBookingConfirmation = function(email, numResa, nameLogement, price, startDate, endDate) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${email}` ,
        subject: 'Confirmation de votre réservation',
        html: `
      <h3> Bonjour  </h3>
      <p>Merci pour votre demande de réservation n° ${numResa} concernant le logement ${nameLogement}  d'un montant de ${price} euros du ${startDate.toLocaleDateString()} au ${endDate.toLocaleDateString()}  </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }

    return sendEmail(message);
}
exports.sendBookingConfirmationOwner = function(email, numResa, nameLogement, price, startDate, endDate, nameRent, firstNameRent, emailRent) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${email}` ,
        subject: 'Nouvelle réservation',
        html: `
      <h3> Bonjour  </h3>
      <p>Bonjour une réservation a eu lieu concernant votre logement ${nameLogement}  d'un montant de ${price} euros du ${startDate.toLocaleDateString()} au ${endDate.toLocaleDateString()}  </p>
      <p>Cette réservation a eu lieu par ${nameRent} ${firstNameRent} <br/> email : ${emailRent}  </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendEmailContact = function(emailUser, content) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: 'shai@dsp-dev019-sh-ri-de.fr' ,
        subject: 'Nouveau message ',
        html: `
      <h3> Bonjour  </h3>
      <p>Bonjour vous avez un nouveau message de ${emailUser} voici le contenu :  </p>
      <p> ${content} </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendEmailBlock = function(email, firstName, lastName) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${email}` ,
        subject: 'Votre compte atypikHouse à été bloqué ',
        html: `
      <h3>Bonjour</h3>
      <p> ${firstName} ${lastName} votre compte  a été bloqué pour non respect des conditons générales d'utilisation</p>
      <p> Veuillez contacter l'administrateur du site via le formulaire de contact du site internet </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendCreateCategoryMail = function(userName, userFirstName, email, categoryName ) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${email}` ,
        subject: 'Une nouvelle catégorie a été créée',
        html: `
      <h3> Bonjour  </h3>
      <p>Bonjour ${userName} ${userFirstName} une nouvelle catégorie : ${categoryName} a été ajoutée     </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendCreateEquipementMail = function(userName, userFirstName, email, equipementName ) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${email}` ,
        subject: 'Un nouveau équipement a été créé',
        html: `
      <h3> Bonjour  </h3>
      <p>Bonjour ${userName} ${userFirstName} un nouveau équipement : ${equipementName} a été ajouté     </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}
exports.sendBannedMail = function({toUserMail, toUserLastName, toUserFirstName, content}) {
    const message = {
        from: 'shai@dsp-dev019-sh-ri-de.fr',
        // to: toUser.email // in production uncomment this
        to: `${toUserMail}`,
        subject: "Non respect des conditions d'utilisation",
        html: `
      <h3> Bonjour ${toUserLastName} ${toUserFirstName} </h3>
      <p>Votre compte à été désactivé pour les raisons suivante :</p>
      <p> ${content} </p>
      <p>Cordialement</p>
      <p>L'équipe AtypikHouse</p>
    `
    }
    return sendEmail(message);
}

