const Sequelize = require("sequelize");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

let corsOptions = {
	origin: ["https://dsp-dev019-sh-ri-de.fr", "http://localhost:3000"], default: "https://dsp-dev019-sh-ri-de.fr"
	//origin: ["https://dsp-dev019-sh-ri-de.fr"]
	//origin: ["http://localhost:3000"]
};


app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {

	//const origin = cors.origin.indexOf(req.header('origin').toLowerCase()) > -1 ? req.headers.origin : cors.default;
	let origin = req.headers.origin;
	if (corsOptions.origin.indexOf(origin) >= 0) {
		res.setHeader("Access-Control-Allow-Origin", origin);
	}
	// Website you wish to allow to connect
	//res.setHeader('Access-Control-Allow-Origin', origin);
	//res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});


// database
const db = require("./app/models");
const Role = db.role;
const User = db.user;
const Posts = db.post;
const Category = db.category;
const House = db.house;
const Booking = db.booking;
const Comment = db.comment;
const Equipment = db.equipment;

const DynamicPropertiesHouse = db.dynamicPropertiesHouse


db.sequelize.sync();
// force: true will drop the table if it already exists
/*db.sequelize.sync({force: true}).then(() => {
	console.log('Drop and Resync Database with { force: true }');
	initial();
});*/

// simple route
app.get("/", (req, res) => {
	res.json({ message: "Welcome to Atypic application." });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/booking.routes')(app);
require('./app/routes/category.routes')(app);
require('./app/routes/house.routes')(app);
require('./app/routes/admin.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
module.exports = app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);

});

async function initial() {
	const role1 = await Role.create({
		id: 1,
		name: "user"
	});

	await Role.create({
		id: 2,
		name: "moderator"
	});

	await Role.create({
		id: 3,
		name: "admin"
	});

	const user1 = await User.create({
		firstName: "Test",
		lastName: "Test",
		email: "Test",
		password: "Test",
		street: "rue de la paix",
		postalCode: "90888",
		city: "citytest",
		country: "countrytest",
		roles: ["admin", "moderator"]
	});

	await user1.setRoles(2);
	await user1.setRoles(3);

	await User.create({
		firstName: "Ruben",
		lastName: "Shai",
		email: "shairuben@gmail.com",
		password: "Test",
		street: "Rue de la paix",
		postalCode: "92001",
		city: "Boulogne",
		country: "France",
		roles: "moderator"
	});

	await Posts.create({
		title: "toto",
		content: "tata",
		image: "titi",
		userId: 1
	});

	await Category.create({
		name: "Les roulottes"
	});

	await Category.create({
		name: "Les caravanes"
	});

	await Category.create({
		name: "Les campings"
	});

	const house1 = await House.create({
		name: "La maison de la vie",
		receptionCapacity: 3,
		price: 40,
		nbBed: 3,
		status: 0,
		street: "Rue de la vie",
		postalCode: "43333",
		city: "Saint jean de luz",
		country: "France",
		userId: 1,
		categoryId: 1
	});

	const house2 = await House.create({
		name: "La maison de Mickey",
		receptionCapacity: 3,
		price: 40,
		nbBed: 3,
		status: 1,
		street: "Rue de Disney",
		postalCode: "75002",
		city: "Paris",
		country: "France",
		userId: 2,
		categoryId: 2
	});

	await Booking.create({
		price: 10,
		startDate: "10/10/2020",
		endDate: "11/10/2020",
		userId: 1,
		houseId: 1
	});

	await Comment.create({
		rate: 3,
		valid: 1,
		userId: 1,
		bookingId: 1,
		houseId: 1
	});

	const equipment1 = await Equipment.create({
		name: "Piscine"
	});

	const equipment2 = await Equipment.create({
		name: "Radiateur"
	});

	const equipment3 = await Equipment.create({
		name: "Chauffage"
	});

	const equipment4 = await Equipment.create({
		name: "Climatisation"
	});


	//await house1.setEquipments(2);
	//await house1.addEquipment(equipment1, {through: {selfGranted: false}})
	await house1.addEquipment(equipment1);
	await house1.addEquipment(equipment2);
	await house1.addEquipment(equipment3);
	await house1.addEquipment(equipment4);

	await house2.addEquipment(equipment2);
	await house2.addEquipment(equipment4);

	const user = await User.findOne({ where: { id: 1 } });
	await user.setRoles(2);
	//await equipment1.setHouses(2);
	////	await user1.setRoles(3);
}
