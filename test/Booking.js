let chai  = require("chai");
let chaiHttp = require('chai-http')
let server = require("../server")

chai.should();

chai.use(chaiHttp);

describe ('AtypikHouse API BOOKING', () => {

    var token;

    describe("POST /api/auth/signIn",()=>{
        it("It should POST a new User",(done)=>{
            const user = {
                email : "shaihaddad02@gmail.com",
                password: "1234"
            };
            chai.request(server)
                .post("/api/auth/signin")
                .send(user)
                .end((err,response) =>{
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id');
                    response.body.should.have.property('firstName');
                    response.body.should.have.property('lastName');
                    response.body.should.have.property('street');
                    response.body.should.have.property('postalCode');
                    response.body.should.have.property('city');
                    response.body.should.have.property('country');
                    response.body.should.have.property('email');
                    response.body.should.have.property('roles');
                    response.body.should.have.property('accessToken');
                    token = response.body.accessToken;
                    console.log("la response " + response)
                    done();
                });
        });
    });


    describe ("GET /api/booking/me", () => {
        it('It should GET all booking by user Id',  (done) => {
            const idCategory = 13;
            chai.request(server)
                .get("/api/booking/me" )
                .set('x-access-token',token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array')
                    done();
                })
        });

    })









})