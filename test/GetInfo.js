let chai  = require("chai");
let chaiHttp = require('chai-http')
let server = require("../server")

chai.should();

chai.use(chaiHttp);


describe ('AtypikHouse API GET', () => {

    // Category Test

    // Recover all category
    describe ("GET /api/category", () => {
        it('It should GET all the category',  (done) => {
            chai.request(server)
                .get("/api/category")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        });
    })

    // Recover all category by id
    describe ("GET /api/category:id", () => {
        it('It should GET all the category by id',  (done) => {
            const idCategory = 1;
            chai.request(server)
                .get("/api/category/" + idCategory )
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        });

    })

    // Recover all house by categorie id
    describe ("GET /categoryHouse/:id", () => {
        it('It should GET all house by category ID',  (done) => {
            const idCategory = 1;
            chai.request(server)
                .get("/api/categoryHouse/:id" + idCategory )
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        });

    })

    describe ("GET /api/equipments", () => {
        it('It should GET all the equipment',  (done) => {
            chai.request(server)
                .get("/api/equipments")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        });
    })




} )